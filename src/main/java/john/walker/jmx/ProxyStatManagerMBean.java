package john.walker.jmx;

public interface ProxyStatManagerMBean {

	/**
	 * 启用代理
	 */
	void close();

	/**
	 * 关闭代理
	 */
	void open();

}
